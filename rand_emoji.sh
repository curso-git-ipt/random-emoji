#!/usr/bin/env bash

rand_emoji=$(shuf -i 128512-128591 -n1)
rand_emoji_code=$(printf '%x' $rand_emoji)
echo -e "\U${rand_emoji_code}"
